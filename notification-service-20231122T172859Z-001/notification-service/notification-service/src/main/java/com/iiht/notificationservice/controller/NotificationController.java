package com.iiht.notificationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.iiht.notificationservice.dto.CourseRegistrationDto;
import com.iiht.notificationservice.listeners.MessageProducer;

import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping(value="/notify")
@Slf4j
public class NotificationController {
	
	@Autowired
    private MessageProducer messageProducer;

	@PostMapping
	public ResponseEntity<CourseRegistrationDto> registerCourse(@RequestBody CourseRegistrationDto courseRegistrationDto){
		
		CourseRegistrationDto response = new CourseRegistrationDto ();

		try {
			
			String json = new Gson().toJson(courseRegistrationDto);
			
	        messageProducer.sendMessage("test", json);

	        
			response.setMessage("Success");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Unable to register course",e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
	

}
