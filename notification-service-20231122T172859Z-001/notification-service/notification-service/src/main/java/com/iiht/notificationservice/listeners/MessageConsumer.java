package com.iiht.notificationservice.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.iiht.notificationservice.service.MailService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j

public class MessageConsumer {

	@Autowired private MailService mailService;
	
    @KafkaListener(topics = "test", groupId = "test-group")
    public void listen(String message){
        System.out.println("Received message: " + message);
        try {
        mailService.sendMultipleMailsthroughHtml(message);
        }
        catch (Exception e) {
        	log.error ("unable to send message");
        }
  
        
    }

}
