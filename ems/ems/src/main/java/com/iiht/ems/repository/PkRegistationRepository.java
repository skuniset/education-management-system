package com.iiht.ems.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.ems.model.CompositePrimaryKey;
import com.iiht.ems.model.PkRegistration;

@Repository
public interface PkRegistationRepository extends CrudRepository<PkRegistration, CompositePrimaryKey>{

}
