package com.iiht.ems.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Payload;

import jakarta.validation.Constraint;

@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidateRegistaration.class)
public @interface RegistrationValidation {
	String message() default "Must be 8 characters long and combination of uppercase letters, lowercase letters, numbers, special characters.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
