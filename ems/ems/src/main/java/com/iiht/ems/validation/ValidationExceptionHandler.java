package com.iiht.ems.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.iiht.ems.exception.RegistrationNotFoundException;

import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ValidationExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<?> notValid(MethodArgumentNotValidException ex, HttpServletRequest request) {
    List<String> errors = new ArrayList<>();

    //ex.getBindingResult().getFieldErrors().forEach(err -> errors.add(err.getDefaultMessage())));)
    ex.getAllErrors().forEach(err -> errors.add(err.getDefaultMessage()));

    Map<String, List<String>> result = new HashMap<>();
    result.put("errors", errors);

    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
  }
  
  @ExceptionHandler(RegistrationNotFoundException.class)
  public ResponseEntity<?> notValid(RegistrationNotFoundException ex, HttpServletRequest request) {


    Map<String,String> result = new HashMap<>();
    result.put("errors", ex.getLocalizedMessage());

    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
  }
}