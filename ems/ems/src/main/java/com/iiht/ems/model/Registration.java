package com.iiht.ems.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "registration")
@EqualsAndHashCode(callSuper = true)
@DynamicInsert
//Specifies that SQL insert statements for the annotated entityare generated dynamically, 
//and only include the columns to which anon-null value must be assigned. 
@DynamicUpdate
/*
 * Specifies that SQL update statements for the annotated entityare generated
 * dynamically, and only include columns which are actuallybeing updated.
 */

@Data
@Audited
public class Registration extends BaseEntity{

	@NotNull
	private String email;
	private  String course;
}
