package com.iiht.ems.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InputRequest<T> {

	private String user;
	private T classType ;
}
