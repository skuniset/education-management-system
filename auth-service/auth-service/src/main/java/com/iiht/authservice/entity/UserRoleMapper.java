package com.iiht.authservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "user_role_mapper")
@EqualsAndHashCode(callSuper = true)
public @Data class UserRoleMapper extends BaseEntity {

	@Column(columnDefinition = "tinyint(1) default 0")
	private Boolean isManager = false;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private User user;
	
	@ManyToOne(targetEntity = Role.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId")
	private Role role;
	
	public UserRoleMapper() {}
	
	public UserRoleMapper(User user, Role role) {
		super();
		this.user = user;
		this.role = role;
	}
	
}
