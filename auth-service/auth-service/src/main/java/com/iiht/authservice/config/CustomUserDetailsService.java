package com.iiht.authservice.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.iiht.authservice.entity.Role;
import com.iiht.authservice.entity.UserRoleMapper;
import com.iiht.authservice.repository.UserRepository;
import com.iiht.authservice.repository.UserRoleMapperRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserRoleMapperRepository userRoleMapperRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<com.iiht.authservice.entity.User> userFromDb = userRepository.findByEmail(username);

		if (userFromDb.isPresent()) {
			
			
			com.iiht.authservice.entity.User loggedInUser = userFromDb.get();
			
			return  new org.springframework.security.core.userdetails.User(loggedInUser.getEmail(),
					loggedInUser.getPassword(), getGrantedAuthorities(loggedInUser));
		} else {
			throw new UsernameNotFoundException("Invalid username or password.");
		}

	}

	public Collection<? extends GrantedAuthority> getGrantedAuthorities(
			com.iiht.authservice.entity.User loggedInUser) {
		Collection<GrantedAuthority> authorities = new HashSet<>();

		System.out.println("gettinng roles and permissions");
		List<UserRoleMapper> userRoleMappers = userRoleMapperRepository
				.getUserRolesByUserRoleMapper(loggedInUser.getEmail());
		System.out.println(new Gson().toJson(userRoleMappers));
		Collection<Role> roles = userRoleMappers.stream().map(UserRoleMapper::getRole).toList();
		System.out.println(new Gson().toJson(roles));

		authorities.addAll(getAuthorities(roles));
		System.out.println(new Gson().toJson(authorities));

		System.out.println(roles);
		return authorities;

	}
	
	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		privileges.forEach(privilege -> grantedAuthorities.add(new SimpleGrantedAuthority(privilege)));
		return grantedAuthorities;
	}
	
	private List<String> getPrivileges(Collection<Role> roles) {
		List<String> privileges = new ArrayList<>();
		/**List<Long> roleIds = roles.stream().map(Role::getId).collect(Collectors.toList());
		List<RolePermissionMapper> mappers = userService.getRolePermissionMapperByRoleId(roleIds);**/
		roles.forEach(mapper -> privileges.add("ROLE_"+mapper.getCode().toUpperCase()));
		return privileges;
	}
	
	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
		return getGrantedAuthorities(getPrivileges(roles));
	}

}
