package com.iiht.authservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "role_permission_mapper")
@EqualsAndHashCode(callSuper = true)
public @Data class RolePermissionMapper extends BaseEntity {

	@ManyToOne(targetEntity = Role.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId")
	private Role role;
	
	@ManyToOne(targetEntity = Permission.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "permissionId")
	private Permission permission;
	
	public RolePermissionMapper() {}
	
	public RolePermissionMapper(Role role, Permission permission) {
		super();
		this.role = role;
		this.permission = permission;
	}
	
}
