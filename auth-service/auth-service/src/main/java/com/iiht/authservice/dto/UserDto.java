package com.iiht.authservice.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

public class UserDto extends BaseDto{

    private String password;
    private String email;
    private String token;
    private String mobile;
    private String city;
    
    
  
}
