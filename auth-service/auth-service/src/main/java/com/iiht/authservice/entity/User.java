package com.iiht.authservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity{

  
    private String name;
    private String email;
    private String password;
    private String mobile;
    private String city;
    
   
    
   }
