package com.iiht.authservice.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.iiht.authservice.dto.UserDto;
import com.iiht.authservice.entity.Role;
import com.iiht.authservice.entity.User;
import com.iiht.authservice.entity.UserRoleMapper;
import com.iiht.authservice.exception.UserAlreadyExistsException;
import com.iiht.authservice.repository.RoleRepository;
import com.iiht.authservice.repository.UserRepository;
import com.iiht.authservice.repository.UserRoleMapperRepository;
import com.iiht.authservice.service.UserServie;

@Service
public class UserServiceImpl implements UserServie{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRoleMapperRepository userRoleMapperRepository;
	
	  @Autowired
	    private PasswordEncoder passwordEncoder;

	@Override
	public void saveUser(UserDto userDto) {
		
		Optional<User> existingUser = userRepository.findByEmail(userDto.getEmail());
		
		if (existingUser.isPresent()) {
			throw new UserAlreadyExistsException("Already Registred, Please login to select course");
		}
		
		User user = new User();
		user.setEmail(userDto.getEmail());
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		user.setMobile(userDto.getMobile());
		user.setCity(userDto.getCity());
		User userRegistered = userRepository.save(user);
		
		UserRoleMapper userRoleMapper = new UserRoleMapper();
		Role role = roleRepository.findByName("USER");
		userRoleMapper.setRole(role);
		userRoleMapper.setUser(userRegistered);
		userRoleMapperRepository.save(userRoleMapper);
		
	}

	
}
