package com.iiht.registrationservice.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.iiht.registrationservice.dto.CourseDto;
import com.iiht.registrationservice.exception.CourseExistExeption;
import com.iiht.registrationservice.model.Course;
import com.iiht.registrationservice.repository.CourseRepository;
import com.iiht.registrationservice.serviceImpl.CourrseServiceImpl;

@ExtendWith(MockitoExtension.class)
public class CourseServiceTest {
	
	@InjectMocks @Spy
	private CourseService courseService = new CourrseServiceImpl();
	
	@Mock private CourseRepository courseRepository;
	
	private Course course;
	private CourseDto courseDto;
	
	@BeforeEach
	void init () {
		
		 course = new Course();
		course.setId(1L);
		course.setName("name");
		course.setCode("code");
		
		 courseDto = new CourseDto();
		courseDto.setId(1L);
		courseDto.setName("name");
		courseDto.setCode("code");
	}
	
	@Test
	void when_addCourseMethod_isCalled_then_newCourseShouldBeAddedToDb() {
		
		doReturn(Optional.empty()).when(courseRepository).findByName(courseDto.getName().toLowerCase());
		doReturn(course).when(courseRepository).save(any(Course.class));
		Course expeted = courseService.addCourse(courseDto);
		assertNotNull(expeted);
		verify(courseRepository,times(1)).findByName(courseDto.getName().toLowerCase());
		
	}
	
	@Test
	void when_addCourseMethod_with_already_registredCosrseName_isCalled_then_newCourseShouldBeAddedToDb() {
		
		doReturn(Optional.of(course)).when(courseRepository).findByName(courseDto.getName().toLowerCase());
		
		Executable executable = () -> courseService.addCourse(courseDto);

		assertThrows(CourseExistExeption.class, executable);
		verify(courseRepository,times(1)).findByName(courseDto.getName().toLowerCase());
		
	}
	
	

}
