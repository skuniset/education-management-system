package com.iiht.registrationservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.registrationservice.model.Facuility;

@Repository
public interface FaculityRepository extends CrudRepository<Facuility, Long>{

}
