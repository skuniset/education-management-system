package com.iiht.registrationservice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "facuility")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Facuility extends BaseEntity{
	
	private String name;
	private String email;
	
	
	@EqualsAndHashCode.Exclude
	@OneToOne(targetEntity = Course.class)
	@JoinColumn(name = "courseId")
	private Course course;
	

}