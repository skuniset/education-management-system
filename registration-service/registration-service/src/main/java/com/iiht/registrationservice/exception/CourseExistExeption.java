package com.iiht.registrationservice.exception;

public class CourseExistExeption  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CourseExistExeption(String message) {
		super(message, new Throwable());
	}

}
