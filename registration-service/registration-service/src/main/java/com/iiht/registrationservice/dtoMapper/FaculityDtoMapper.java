package com.iiht.registrationservice.dtoMapper;

import com.iiht.registrationservice.dto.FacalityDto;
import com.iiht.registrationservice.model.Facuility;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FaculityDtoMapper {

	public static FacalityDto toFaculityDto(Facuility faculity) {

		FacalityDto faculityDto = new FacalityDto();
		faculityDto.setId(faculity.getId());
		faculityDto.setName(faculity.getName());
		faculityDto.setEmail(faculity.getEmail());
		return faculityDto;

	}

	public static Facuility toFaculity(FacalityDto faculitydto) {

		Facuility facality = new Facuility();
		facality.setName(faculitydto.getName());
		facality.setEmail(faculitydto.getEmail());
		return facality;

	}
}
