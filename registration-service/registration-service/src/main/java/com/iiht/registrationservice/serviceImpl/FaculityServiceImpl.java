package com.iiht.registrationservice.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.iiht.registrationservice.dto.FacalityDto;
import com.iiht.registrationservice.exception.CourseExistExeption;
import com.iiht.registrationservice.model.Course;
import com.iiht.registrationservice.model.Facuility;
import com.iiht.registrationservice.repository.CourseRepository;
import com.iiht.registrationservice.repository.FaculityRepository;
import com.iiht.registrationservice.service.FaculityService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FaculityServiceImpl implements FaculityService {
	
	private final FaculityRepository faculityRepository;
	private final CourseRepository courseRepository;

	@Override
	public Facuility addFaaculity(FacalityDto faculitydto) {
		
		final Facuility facality = new Facuility();
		facality.setName(faculitydto.getName());
		facality.setEmail(faculitydto.getEmail());
		Optional<Course> optionalCourse = courseRepository.findByName(faculitydto.getCourseName());
		if (optionalCourse.isPresent()) {
			facality.setCourse(optionalCourse.get());
			return faculityRepository.save(facality);
		}
		else {
			throw new CourseExistExeption("Course not exists with name "+ faculitydto.getName());

		}
	}

	@Override
	public List<Facuility> getListByCourseId(Long courseId) {
		// TODO Auto-generated method stub
		return null;
	}

}
