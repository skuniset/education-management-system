package com.iiht.registrationservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CourseDto extends BaseDto{
	
	private String code;
	private String name;
	private List<CourseDto> courseDto;

}
