package com.iiht.registrationservice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "course_faculity_mapper")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CourseFaculityMapper extends BaseEntity{

	@OneToOne(targetEntity = Course.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "courseId")
	private Course course;

	@OneToOne(targetEntity = Facuility.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "facuilityId")
	private Facuility facuility;
}
