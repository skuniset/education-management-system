/*package com.iiht.registrationservice.security;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.iiht.authservice.config.CustomUserDetailsService;
import com.iiht.authservice.config.JwtAuthenticationEntryPoint;
import com.iiht.authservice.config.JwtRequestFilter;
import com.iiht.authservice.config.JwtTokenUtil;

import lombok.AllArgsConstructor;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@AllArgsConstructor
public class SecurityConfig {
	
	private final JwtRequestFilter jwtRequestFilter;
	
	private final  JwtTokenUtil jwtTokenUtil;
    
    private final  JwtAuthenticationEntryPoint entryPoint;



    @Bean
    public UserDetailsService userDetailsService(){
        return new CustomUserDetailsService();
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
    	
    	httpSecurity.csrf(AbstractHttpConfigurer::disable);

    	  httpSecurity
          
                .authorizeHttpRequests((authz) -> authz
                    .requestMatchers("/register").permitAll()
                   .requestMatchers("/course","/facality").hasAnyAuthority("ROLE_ADMIN")
                    .anyRequest().authenticated()
                    
                );
    	  httpSecurity.authenticationProvider(authenticationProvider());

    	//httpSecurity.addFilter(new JwtUsernameAndPasswordAuthenticationFilter());

  		httpSecurity.addFilterBefore (jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

            return httpSecurity.build();
        
    }
    
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
    	return (web) -> web.ignoring().requestMatchers("/register", "/authenticate");
    }



	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationProvider=new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
    	return  config.getAuthenticationManager();
    }
}*/
