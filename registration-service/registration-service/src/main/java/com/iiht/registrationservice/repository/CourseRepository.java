package com.iiht.registrationservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.iiht.registrationservice.model.Course;

//to check the reision histroy on db we extend RevisonRepo of concept springdataEvn
@Repository
public interface CourseRepository extends CrudRepository<Course, Long> ,RevisionRepository<Course, Long, Integer>{

	Optional<Course>  findByName(String name);

}
