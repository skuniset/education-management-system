package com.iiht.registrationservice.service;

import com.iiht.registrationservice.dto.CourseDto;
import com.iiht.registrationservice.model.Course;

public interface CourseService {
	
	public Course addCourse(CourseDto courseDto);
	public void deleteCourse(Long couseId);

}
