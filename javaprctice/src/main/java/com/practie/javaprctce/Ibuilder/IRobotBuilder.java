package com.practie.javaprctce.Ibuilder;

public interface IRobotBuilder {

	public void buildHands(String heaf);
	public void buildLegs(String legs);
	public void buildHead(String head);
	
}
