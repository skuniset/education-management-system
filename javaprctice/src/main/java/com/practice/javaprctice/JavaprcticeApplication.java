package com.practice.javaprctice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaprcticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaprcticeApplication.class, args);
	}

}
